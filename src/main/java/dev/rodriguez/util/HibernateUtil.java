package dev.rodriguez.util;


import dev.rodriguez.model.Author;
import dev.rodriguez.model.BookItem;
import dev.rodriguez.model.Feedback;
import dev.rodriguez.model.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * establishes connections to database
 */


public class HibernateUtil {

    private static SessionFactory sessionFactory;

    private static final Logger logger = LoggerFactory.getLogger(HibernateUtil.class);

    private static SessionFactory getSessionFactory(){
        if(sessionFactory == null) {
            try {

                Configuration configuration = new Configuration();
                Properties settings = new Properties();

                // Properties that are database specific:
                settings.put(Environment.URL, System.getenv("DB_URL"));      // JDBC URL
                settings.put(Environment.USER, System.getenv("DB_USER"));    // database user
                settings.put(Environment.PASS, System.getenv("DB_PASS"));    // database password

                //https://docs.microsoft.com/en-us/sql/connect/jdbc/working-with-a-connection?view=sql-server-ver15
                settings.put(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");   //JDBC driver class

                /**
                 * the Dialect used by Hibernate to talk to the database
                 * The class name of a Hibernate org.hibernate.dialect.Dialect
                 * which allows Hibernate to generate SQL optimized for a particular relational database
                 */
                settings.put(Environment.DIALECT, "org.hibernate.dialect.SQLServerDialect");

                settings.put(Environment.HBM2DDL_AUTO, "update");
                settings.put(Environment.SHOW_SQL, "true");

                configuration.setProperties(settings);

                /**
                 * provide hibernate mappings to configuration
                 */
                configuration.addAnnotatedClass(Author.class);
                configuration.addAnnotatedClass(BookItem.class);

                configuration.addAnnotatedClass(Feedback.class);

                configuration.addAnnotatedClass(User.class);


                sessionFactory = configuration.buildSessionFactory();
                logger.info("Hibernate successfully configured!");
            } catch (HibernateException ex) {
                logger.warn("Error: Hibernate unsuccessfully configured!");
                throw new HibernateException(ex);
            }
        }
        return sessionFactory;
    }

    // invoke this method to get an instance of an object implementing Session
    public static Session getSession(){
        return getSessionFactory().openSession();
    }

}
