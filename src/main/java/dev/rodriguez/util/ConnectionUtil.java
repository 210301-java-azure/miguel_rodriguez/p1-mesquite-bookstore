package dev.rodriguez.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * represents a session with a database
 * the connection object establishes connections with a database to execute SQL statements against it
 * in addition, perform other operations such as commits and rollbacks
 */
import java.sql.Connection;
// allows us to establish a connection to the database and manage a set of JDBC drivers
import java.sql.DriverManager;
// which can be thrown when we perform operations against a relational database
import java.sql.SQLException;

import java.util.Properties;


public class ConnectionUtil {

    private static Connection connection;

    private static Logger logger = LoggerFactory.getLogger(ConnectionUtil.class);

    private ConnectionUtil() {
        super();
    }

    public static Connection getConnection() throws SQLException {
        try {

            if (connection == null || connection.isClosed()) {


                String connectionUrl = System.getenv("connectionUrl");
                String username = System.getenv("username");
                String password = System.getenv("password");

                connection = DriverManager.getConnection(connectionUrl, username, password);
                logger.info("The connection has been successfully established!");
            }
        } catch (SQLException ex) {
            logger.warn("A connection error has occurred!");
            ex.printStackTrace();
        }
        return connection;
    }

}
