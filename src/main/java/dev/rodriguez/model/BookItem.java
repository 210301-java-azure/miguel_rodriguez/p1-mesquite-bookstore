package dev.rodriguez.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import dev.rodriguez.customtype.CustomBookCategoryType;
import dev.rodriguez.customtype.CustomBookConditionType;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * essentially maps to the book table using annotations
 */


@Entity
@Table(name="book")
@JsonIgnoreProperties("hibernateLazyInitializer")
@FilterDef(name = "minFilter", parameters = {
        @ParamDef(name="priceParam", type="double")
})
@FilterDef(name = "maxFilter", parameters = {
        @ParamDef(name="priceParam", type="double")
})
@Filters({
        @Filter(name="minFilter", condition = "price > :priceParam"),
        @Filter(name="maxFilter", condition = "price < :priceParam")
})
@TypeDefs({
        @TypeDef(name="customBookCategoryType", typeClass= CustomBookCategoryType.class),
        @TypeDef(name="customBookConditionType", typeClass= CustomBookConditionType.class)
})

public class BookItem implements Serializable {

    /**
     * v1:
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="book_id")
    private int bookID;
    @Column(name="title")
    private String bookTitle;
    @Column(name="price")
    private double price;

    @Column(name="book_isbn")
    private String bookISBN; //ideally, an isbn-13; however, isbn-5

    //@Column(name="author")
    @OneToOne
    //@JoinColumn(name = "author_id")
    private Author author;      //ideally, possible authors; however, one author

    @Type(type = "customBookCategoryType")
    @Column(name="book_category")
    private BookCategory category;

    @Type(type = "customBookConditionType")
    @Column(name="book_condition")
    private BookCondition condition;

    /**
     * v1:
     *
     *     private BookCategory category;
     *     private BookCondition condition;
     *
     */

    public BookItem() {
        super();
    }

    public BookItem(int bookID) {
        this.bookID = bookID;
    }

    public BookItem(int bookID, double price) {
        this.bookID = bookID;
        this.price = price;
    }

    public BookItem(String bookTitle, double price) {
        this.bookTitle = bookTitle;
        this.price = price;
    }

    public BookItem(int bookID, String bookTitle, double price) {
        this.bookID = bookID;
        this.bookTitle = bookTitle;
        this.price = price;
    }

    public BookItem(int bookID, String bookTitle, double price, Author author) {
        this.bookID = bookID;
        this.bookTitle = bookTitle;
        this.price = price;
        this.author = author;
    }

    public BookItem(int bookID, String bookTitle, double price, String bookISBN, Author author) {
        this.bookID = bookID;
        this.bookTitle = bookTitle;
        this.price = price;
        this.bookISBN = bookISBN;
        this.author = author;
    }

    public BookItem(int bookID, String bookTitle, double price, String bookISBN, Author author, BookCategory category, BookCondition condition) {
        this.bookID = bookID;
        this.bookTitle = bookTitle;
        this.price = price;
        this.bookISBN = bookISBN;
        this.author = author;
        this.category = category;
        this.condition = condition;
    }

    public int getBookID() {
        return bookID;
    }

    public void setBookID(int bookID) {
        this.bookID = bookID;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * vn:
     */
    public String getBookISBN() {
        return bookISBN;
    }

    public void setBookISBN(String bookISBN) {
        this.bookISBN = bookISBN;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }


    public BookCategory getCategory() {
        return category;
    }

    public void setCategory(BookCategory category) {
        this.category = category;
    }

    public BookCondition getCondition() {
        return condition;
    }

    public void setCondition(BookCondition condition) {
        this.condition = condition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookItem item = (BookItem) o;
        return bookID == item.bookID && Double.compare(item.price, price) == 0 && Objects.equals(bookTitle, item.bookTitle) && Objects.equals(bookISBN, item.bookISBN) && Objects.equals(author, item.author) && category == item.category && condition == item.condition;
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookID, bookTitle, price, bookISBN, author, category, condition);
    }

    @Override
    public String toString() {
        return "BookItem{" +
                "bookID=" + bookID +
                ", bookTitle='" + bookTitle + '\'' +
                ", price=" + price +
                ", bookISBN='" + bookISBN + '\'' +
                ", author=" + author +
                ", category=" + category +
                ", condition=" + condition +
                '}';
    }
}
