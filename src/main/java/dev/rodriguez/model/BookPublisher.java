package dev.rodriguez.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="publisher")
@JsonIgnoreProperties("hibernateLazyInitializer")
public enum BookPublisher {
    PENGUIN, PENGUIN_CLASSICS, MODERN_LIBRARY, OXFORD_UNIVERSITY_PRESS, HARVARD_PRESS, STANFORD_PRESS, CENGAGE, PEARSON;

    public static BookPublisher getBookPublisher(String name) {
        for(BookPublisher publisher: BookPublisher.values()) {
            if(publisher.name().equals(name)){
                return publisher;
            }
        }
        return null;
    }

}
