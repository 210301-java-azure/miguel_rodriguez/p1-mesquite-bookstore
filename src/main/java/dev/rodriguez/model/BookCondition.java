package dev.rodriguez.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="book_condition")
@JsonIgnoreProperties("hibernateLazyInitializer")
public enum BookCondition {

    NEW, GOOD, FAIR, POOR;

    public static BookCondition getBookCondition(String name) {
        for(BookCondition condition: BookCondition.values()) {
            if(condition.name().equals(name)){
                return condition;
            }
        }
        return null;
    }

}
