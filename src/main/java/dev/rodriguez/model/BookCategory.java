package dev.rodriguez.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="category")
@JsonIgnoreProperties("hibernateLazyInitializer")
public enum BookCategory {

    FICTION, NONFICTION, HISTORY, PHILOSOPHY, POETRY, BUSINESS, SCIENCE, ART;

    public static BookCategory getBookCategory(String name) {
        for(BookCategory category: BookCategory.values()) {
            if(category.name().equals(name)){
                return category;
            }
        }
        return null;
    }

}
