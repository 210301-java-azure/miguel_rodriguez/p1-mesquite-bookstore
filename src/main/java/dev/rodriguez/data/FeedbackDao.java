package dev.rodriguez.data;

import dev.rodriguez.model.Feedback;

import java.util.List;

public interface FeedbackDao {

    /**
     * crud method: read
     */
    List<Feedback> getAllFeedback();

    Feedback getIndividualFeedbackByFeedbackID(int feedbackID);


    /**
     * crud method: create
     */
    Feedback addFeedback(Feedback feedback);

    /**
     * crud method: delete
     */
    void deleteFeedback(int feedbackID);

}
