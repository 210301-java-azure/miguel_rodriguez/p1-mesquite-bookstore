package dev.rodriguez.customtype;

import dev.rodriguez.model.BookCondition;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.StringType;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomBookConditionType implements UserType {

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
        return value;
    }

    @Override
    public Serializable disassemble(Object o) throws HibernateException {
        return (BookCondition) deepCopy(o);
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        if(x == y) {
            return true;
        }
        if(x == null || y == null) {
            return false;
        } else {
            return x.equals(y);
        }
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        BookCondition condition = (BookCondition) x;
        return condition.hashCode();
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner) throws HibernateException, SQLException {
        BookCondition condition = null;
        String name = rs.getString(names[0]);   // retrieves an instance of the map class from a JDBC resultSet
        if(name != null) {
            // the actual conversion from string to Enum
            condition = BookCondition.getBookCondition(name);
        }
        return condition;
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
        if(value == null) {
            st.setNull(index, StringType.INSTANCE.sqlType());   // writes an instance of the map class to a preparedStatement
        } else {
            // the conversion from Enum to String
            BookCondition condition = (BookCondition) value;
            st.setString(index, condition.name());
        }
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }


    @Override
    public int[] sqlTypes() {
        return new int[] {StringType.INSTANCE.sqlType()};
    }

    @Override
    public Class returnedClass() {
        return CustomBookConditionType.class;
    }
}
