package dev.rodriguez.services;

import dev.rodriguez.data.FeedbackDao;
import dev.rodriguez.data.FeedbackDaoHibImpl;
import dev.rodriguez.model.Feedback;

import java.util.List;

public class FeedbackService {

    private final FeedbackDao feedbackDao = new FeedbackDaoHibImpl();

    // crud method: read
    public List<Feedback> getAllFeedback() {
        return feedbackDao.getAllFeedback();
    }

    // crud method: read
    public Feedback getIndividualFeedbackByFeedbackID(int feedbackID) {
        return feedbackDao.getIndividualFeedbackByFeedbackID(feedbackID);
    }

    // crud method: create
    public Feedback addFeedback(Feedback feedback) {
        return feedbackDao.addFeedback(feedback);
    }

    // crud method: delete
    public void removeFeedback(int feedbackID) {
        feedbackDao.deleteFeedback(feedbackID);
    }

}
