package dev.rodriguez.services;


import dev.rodriguez.data.UserDao;
import dev.rodriguez.data.UserDaoHibImpl;
import dev.rodriguez.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * business logic:
 *
 */

public class UserService {

    private final UserDao userDao = new UserDaoHibImpl();

    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    // crud method: read
    public List<User> getAllBooks() {
        return userDao.getAllUsers();
    }

    // crud method: read
    public User getUserByUserID(int userID) {
        return userDao.getUserByUserID(userID);
    }

    // crud method: read
    public User getUserByUserName(String userName) {
        return userDao.getUserByUserName(userName);
    }

    // crud method: read
    public User getUserByEmail(String email) {
        return userDao.getUserByUserName(email);
    }

    public boolean doesUserExist(String userName) {
        return userDao.doesUserExist(userName);
    }


    public boolean checkPassword(User user) {
        String username = user.getUserName();
        String password = user.getPassword();
        String dbPassword = userDao.getPasswordByUserName(username);
        if (password.equals(dbPassword)) {
            logger.info("Login Attempt. Login successful...");
            return true;
        } else {
            logger.info("Login Attempt. Login unsuccessful...");
            return false;
        }
    }

    public boolean checkPassword(String username, String password) {
        String dbPassword = userDao.getPasswordByUserName(username);
        if (password.equals(dbPassword)) {
            logger.info("Login Attempt. Login successful...");
            return true;
        } else {
            logger.info("Login Attempt. Login unsuccessful...");
            return false;
        }
    }



}
