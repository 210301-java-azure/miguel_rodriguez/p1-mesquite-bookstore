package dev.rodriguez.controlers;

import dev.rodriguez.model.Feedback;
import dev.rodriguez.services.FeedbackService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeedbackController {

    private final Logger logger = LoggerFactory.getLogger(FeedbackController.class);

    private final FeedbackService service = new FeedbackService();

    /**
     * GET request
     */
    public void handleGetAllFeedbackRequest(Context ctx) {
        logger.info("retrieving all records...");
        ctx.json(service.getAllFeedback());
        ctx.status(200);
    }

    /**
     * GET request
     */
    public void handleGetIndividualFeedbackByIDRequest(Context ctx) {
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")) {
            int idInput = Integer.parseInt(idString);
            Feedback feedback = service.getIndividualFeedbackByFeedbackID(idInput);
            if(feedback == null) {
                logger.warn("Error: No feedback found with such id: " + idInput);
                throw new NotFoundResponse("Error: No feedback found with such id: " + idInput);
            } else {
                logger.info("Success: feedback found with id: " + idInput);
                ctx.json(feedback);
            }
        } else {
            throw new BadRequestResponse("Error: {" + idString + "} cannot be parsed.");
        }
    }

    /**
     * POST request
     */
    public void handlePostNewFeedbackItemRequest(Context ctx) {
        Feedback feedback = ctx.bodyAsClass(Feedback.class);
        logger.info("creating new record...");
        service.addFeedback(feedback);
        ctx.status(201);
    }

    /**
     * DELETE request
     */
    public void handleDeleteFeedbackByIDRequest(Context ctx) {
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")) {
            int idInput = Integer.parseInt(idString);
            logger.info("Success: feedback deleted with id: " + idString);
            service.removeFeedback(idInput);
        } else {
            throw new BadRequestResponse("Error: {" + idString + "} cannot be parsed.");
        }
    }

}
