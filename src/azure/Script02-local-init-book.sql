-- init book

insert into book (title, description, publishing_date, publisher, book_condition, price) 
values ('Herodotus the Persian Wars', 
		'History. Ancient History. Military History.', '1947-01-01', 3, 3, 6.65);

insert into book (title, description, publishing_date, publisher, book_condition, price)
values ('The Complete Writings of Thucydides', 
		'History. Ancient History. Military History.', '1934-01-01', 3, 2, 495.00);

insert into book (title, description, publishing_date, publisher, book_condition, price)
values ('Selected Dialogues of Plato', 
		'Philosophy. Ancient Philosophy. Political Philosophy. Epistemology. Metaphysics.', 
		'2001-09-11', 3, 1, 16.00);

insert into book (title, description, publishing_date, publisher, book_condition, price)
values ('The Nicomachean Ethics', 'Ethics. Philosophy. Ancient Philosophy. Moral Philosophy', 
		'2009-07-15', 4, 2, 7.48);


insert into book (title, description, publishing_date, publisher, book_condition, price)
values ('The Selected Poetry of Rainer Maria Rilke', 
		'Existentialism. Poetry. Prose.', '1995-08-01', 3, 2, 13.00);

insert into book (title, description, publishing_date, publisher, book_condition, price)
values ('An Enemy of the People', 'World Literature. Play.', 
		'2010-06-29', 2, 3, 6.50);


insert into book (title, description, publishing_date, publisher, book_condition, price)
values ('The Joyous Science', 'Existentialism. Modern Philosophy.', 
		'2019-03-12', 2, 1, 15.93);

insert into book (title, description, publishing_date, publisher, book_condition, price)
values ('Thus Spoke Zarathustra', 'Existentialism. Modern Philosophy.', 
		'2008-08-14', 4, 2, 7.24);
		
insert into book (title, description, publishing_date, publisher, book_condition, price)
values ('On the Genealogy of Morals', 'Existentialism. Modern Philosophy.', 
		'2009-01-15', 4, 2, 5.98);
		

insert into book (title, description, publishing_date, publisher, book_condition, price)
values ('The 48 Laws of Power', 'Business. Philosophy. Political Philosophy. Social Philosophy.', 
		'2000-09-01', 1, 2, 7.49);
