-- implemented
create table author (
	author_id serial primary key,
	first_name varchar(45) not null,
	middle_name varchar(45) null,
	last_name varchar(45) null,
	last_update timestamp
);

-- implemented
create table category (
	category_id serial primary key,
	name varchar(45) not null,
	last_update timestamp
);

-- implemented
create table book_condition (
	book_condition_id serial primary key,
	name varchar(45) not null,
	last_update timestamp
);

-- implemented
create table publisher (
	publisher_id serial primary key,
	name varchar(45) not null,
	last_update timestamp
);

create table book (
	book_id serial primary key,
	title varchar(90) not null,
	description text,
	publishing_date date not null,
	publisher integer references publisher(publisher_id),
	book_condition integer references book_condition(book_condition_id),
	price numeric(8,2),
	last_update timestamp
);


create table book_author (
	author_id integer,
	book_id integer,
	primary key(author_id, book_id),
	last_update timestamp
);

create table book_category (
	book_id integer,
	category_id smallint ,
	primary key(book_id, category_id),
	last_update timestamp
);


create table staff (
	staff_id serial primary key,
	first_name varchar(45) not null,
	middle_name varchar(45) null,
	last_name varchar(45) not null,
	email varchar(50) not null,
	user_name varchar(16) not null,
	user_password varchar(40) not null,
	last_update timestamp
);

create table store (
	store_id serial primary key,
	manager integer references staff(staff_id),
	last_update timestamp
);
