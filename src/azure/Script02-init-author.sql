-- init author
insert into author (first_name) values ('Aristotle');
insert into author (first_name) values ('Cicero');
insert into author (first_name) values ('Herodotus');
insert into author (first_name) values ('Plato');
insert into author (first_name) values ('Sappho');
insert into author (first_name) values ('Seneca');
insert into author (first_name) values ('Thucydides');
insert into author (first_name) values ('Xenophon');

insert into author (first_name, last_name) values ('David', 'Brooks');
insert into author (first_name, last_name) values ('Albert', 'Camus');
insert into author (first_name, last_name) values ('Robert', 'Greene');
insert into author (first_name, last_name) values ('Cormac', 'McCarthy');

insert into author (first_name, middle_name, last_name) values ('Henrik','Johan', 'Ibsen');
insert into author (first_name, middle_name, last_name) values ('Soren','Aabye', 'Kierkegaard');
insert into author (first_name, middle_name, last_name) values ('Friedrich','Wilhelm', 'Nietzsche');
insert into author (first_name, middle_name, last_name) values ('Rainer','Maria', 'Rilke');
